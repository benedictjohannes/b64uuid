package b64uuid

import (
	"database/sql/driver"
	"encoding/base64"
	"fmt"

	guuid "github.com/google/uuid"
)

var emptyUuidBytes = make([]byte, 16)

type B64Uuid guuid.UUID

// Empty B64Uuid
var Nil = B64Uuid{}

// IsEmpty() tests whether the B64Uuid is empty (all zeros)
func (u *B64Uuid) IsEmpty() bool {
	return *u == Nil
}

/* Convert Outwards */
func (u B64Uuid) String() string {
	if u.IsEmpty() {
		return ""
	}
	uInBytes, err := u.MarshalBinary()
	if err != nil {
		return ""
	}
	return base64.RawURLEncoding.EncodeToString(uInBytes)
}
func (u B64Uuid) MarshalText() ([]byte, error) {
	if u.IsEmpty() {
		return []byte(""), nil
	}
	uInBytes, err := u.MarshalBinary()
	b64str := base64.RawURLEncoding.EncodeToString(uInBytes)
	return []byte(b64str), err
}
func (u B64Uuid) MarshalJSON() ([]byte, error) {
	if u.IsEmpty() {
		return []byte("null"), nil
	}
	uInBytes, err := u.MarshalBinary()
	b64str := base64.RawURLEncoding.EncodeToString(uInBytes)
	return []byte("\"" + b64str + "\""), err
}
func (u B64Uuid) MarshalBinary() ([]byte, error) {
	return u[:], nil
}
func (u B64Uuid) Value() (driver.Value, error) {
	return guuid.UUID(u).Value()
}

// B64ToUUID converts B64Uuid into Google's UUID
func B64ToUUID(b64 B64Uuid) *guuid.UUID {
	gUuid := guuid.UUID(b64)
	return &gUuid
}

// Convert Inwards
func (u *B64Uuid) UnmarshalText(inputJsonBytes []byte) error {
	if string(inputJsonBytes) == "" {
		return u.UnmarshalBinary(emptyUuidBytes)
	}
	inputJsonString := string(inputJsonBytes)
	if len(inputJsonString) != 22 {
		uParsed, err := guuid.Parse(inputJsonString)
		if err != nil {
			return err
		}
		return u.UnmarshalBinary(uParsed[:])
	}
	uuidBytes, err := base64.RawURLEncoding.DecodeString(inputJsonString)
	if err != nil {
		return err
	}
	return u.UnmarshalBinary(uuidBytes)
}
func (u *B64Uuid) UnmarshalJSON(inputJsonBytes []byte) error {
	inputJsonString := string(inputJsonBytes)
	if inputJsonString == "null" || inputJsonString == "\"\"" {
		return u.UnmarshalBinary(emptyUuidBytes)
	}
	if len(inputJsonString) != 24 {
		uParsed, err := guuid.Parse(inputJsonString)
		if err != nil {
			return err
		}
		return u.UnmarshalBinary(uParsed[:])
	}
	inputJsonB64String := inputJsonString[1:23]
	uuidBytes, err := base64.RawURLEncoding.DecodeString(inputJsonB64String)
	if err != nil {
		return err
	}
	err = u.UnmarshalBinary(uuidBytes)
	if err != nil {
		return err
	}
	return nil
}
func (u *B64Uuid) UnmarshalBinary(inputBytes []byte) error {
	if len(inputBytes) != 16 {
		return fmt.Errorf("invalid UUID (got %d bytes)", len(inputBytes))
	}
	copy(u[:], inputBytes)
	return nil
}
func (u *B64Uuid) Scan(src interface{}) (err error) {
	g := guuid.UUID{}
	err = g.Scan(src)
	if err != nil {
		return
	}
	*u = B64FromUuid(g)
	return
}

// B64FromUuid convert Google's UUID into B64Uuid
func B64FromUuid(gUuid guuid.UUID) B64Uuid {
	return B64Uuid(gUuid)
}

/* Create new */

// Parse tries to parse as Base64Url formatted UUID if the string is 22 characters
//
// # When input is not 22 characters, it is passed into underlying Google's UUID type
//
// Empty string immediately results in empty B64Uuid
func Parse(b64uuidStr string) (u B64Uuid, err error) {
	if b64uuidStr == "" {
		return u, nil
	}
	if len(b64uuidStr) != 22 {
		uParsed, err := guuid.Parse(b64uuidStr)
		if err != nil {
			return u, err
		}
		return B64Uuid(uParsed), nil
	}
	uuidBytes, err := base64.RawURLEncoding.DecodeString(b64uuidStr)
	if err != nil {
		return u, err
	}
	err = u.UnmarshalBinary(uuidBytes)
	return u, err
}

// Random creates a random B64UUID by calling underlying Google's UUID's NewRandom()
func NewRandom() B64Uuid {
	randUuid, _ := guuid.NewRandom()
	return B64Uuid(randUuid)
}
