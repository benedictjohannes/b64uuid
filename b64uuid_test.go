package b64uuid

import (
	"bytes"
	"encoding/json"
	"fmt"
	"reflect"
	"testing"

	"github.com/google/uuid"
)

// These should be identical when parsed as UUID
const b64Str = `Rob_sgEhT7-GX9lHmrgVJw`
const uuidStr = `4686ffb2-0121-4fbf-865f-d9479ab81527`

var gUuid = uuid.Must(uuid.Parse(uuidStr))

func TestStrBinaryConv(t *testing.T) {
	b64Uuid, _ := Parse(b64Str)
	gUuidB, _ := gUuid.MarshalBinary()
	b64UuidB, _ := b64Uuid.MarshalBinary()
	if !bytes.Equal(gUuidB, b64UuidB) {
		t.Error("identical uuid in different B64Str and common UUID results in different binary form")
	}
}

func TestUuidInterop(t *testing.T) {
	b64Parsed, _ := Parse(uuidStr)
	gUuidB, _ := gUuid.MarshalBinary()
	b64UuidB, _ := b64Parsed.MarshalBinary()
	if !bytes.Equal(gUuidB, b64UuidB) {
		t.Fatal("B64Uuid failed to parse common UUID")
	}
	b64FromGUuid := B64FromUuid(gUuid)
	b64FromGUuidB, _ := b64FromGUuid.MarshalBinary()
	if !bytes.Equal(b64FromGUuidB, gUuidB) {
		t.Fatal("Conversion from gUUID failed")
	}
	b64ToUuid := B64ToUUID(b64Parsed)
	if gUuid != *b64ToUuid {
		t.Fatal("Conversion to gUUID failed")
	}
}

type structB64UuidWithA struct {
	A B64Uuid `json:"a"`
}

func TestJson(t *testing.T) {
	b64Uuid, _ := Parse(b64Str)
	jsonBytes, err := json.Marshal(b64Uuid)
	if err != nil {
		t.Fatal("error marshalling into JSON", err)
	}
	if string(jsonBytes) != fmt.Sprintf(`"%s"`,b64Str) {
		t.Fatal("json marshal of variable failed")
	}
	var blank B64Uuid
	jsonBytes, err = json.Marshal(blank)
	if err != nil {
		t.Fatal("error marshalling empty B64Uuid into JSON", err)
	}
	if string(jsonBytes) != ("null") {
		t.Fatal("json marshal of variable failed")
	}
	err = blank.UnmarshalJSON([]byte("null"))
	if err != nil {
		t.Fatal("error unmarshaling null into (empty) B64Uuid ", err)
	}
	if !blank.IsEmpty() {
		t.Fatal("json unmarshaling of null result isn't empty B64Uuid")
	}
	var testStructVar structB64UuidWithA
	var resultStructVar structB64UuidWithA
	testStructVar.A, _ = Parse(b64Str)
	jsonBytes, err = json.Marshal(testStructVar)
	if err != nil {
		t.Fatal("error marshalling into JSON", err)
	}
	jsonStr := string(jsonBytes)
	if jsonStr != fmt.Sprintf(`{"a":"%s"}`, b64Str) {
		t.Fatal("json marshaling of struct containing B64Uuid failed")
	}
	err = json.Unmarshal(jsonBytes,&resultStructVar)
	if err != nil {
		t.Fatal("failed to unmarshal from JSON", err)
	}
	if !reflect.DeepEqual(&testStructVar,&resultStructVar){
		t.Fatal("json unmarshaling to struct containing B64Uuid failed")
	}
}
