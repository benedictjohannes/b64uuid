[![Go Reference](https://pkg.go.dev/badge/gitlab.com/benedictjohannes/b64uuid.svg)](https://pkg.go.dev/gitlab.com/benedictjohannes/b64uuid)

This package provides `B64Uuid` type that is essentially renamed _[Google's UUID](https://github.com/google/uuid)_ aimed to have the UUID's string representation encoded as 22 characters URL-safe Base64 while maintaining identical binary format. For example, these are identical UUIDs:

-   `4686ffb2-0121-4fbf-865f-d9479ab81527`
-   `Rob_sgEhT7-GX9lHmrgVJw`

When parsing strings or unmarshalling into text or JSON, if string length does not equaling 22, `B64Uuid` will pass the string to the underlying _Google's UUID_, so both representations of UUID will work correctly.

The more concise Base64URL encoded UUID might be useful when being used in cookies, HTTP REST API etc.

Note that for JSON marshal/unmarshall,

-   Marshalling blank `B64UUID` will result in JSON `null`
-   Unmarshalling JSON `null` would result in blank `B64Uuid`
